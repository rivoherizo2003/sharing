# Build the docker file

```bash
docker build -t oz-node14:1.0 . 
```

# Tag the image
```bash
docker tag oz-node14:1.0 ozirehdocker/oz-node14:1.0
```

# Check if we are connected to dockerhub
```bash
docker login
```

# Push the image to dockerhub
```bash
docker push ozirehdocker/oz-node14:1.0
```

# Run image
```bash
docker run --rm -p 8094:8080 -v ./app.js:/app/app.js ozirehdocker/oz-node14:1.0
```