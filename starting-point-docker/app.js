const express = require('express');

const app = express();

app.get('/', (req, res) => {
  res.send(`
    <h1>Hello world!!!</h1>
    <p>This is a sharing</p>
    <p> about Docker</p>
    <p><h2>1.0</h2></p>
  `);
});

app.get('/error', (req, res) => {
  process.exit(1);
});

app.listen(8080);
